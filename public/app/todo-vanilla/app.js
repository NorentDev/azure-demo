const app = {};

app.start = (parentEl) => {
  const todoList = [];
  let currentList = [];
  let amountTodo = 0;
  let amountDone = 0;
  let showTodo = true;
  let showDone = true;

  //pre-existing HTML elements
  const toggleAll = parentEl.querySelector(".toggle-all");
  const inputTodo = parentEl.querySelector(".input-todo");
  const listTodos = parentEl.querySelector(".list-todos");
  const statusTodos = parentEl.querySelector(".status-todos");
  const showAll = parentEl.querySelector(".show-all");
  const showActive = parentEl.querySelector(".show-active");
  const showCompleted = parentEl.querySelector(".show-completed");

  class todoItem {
    constructor(initialDescription) {
      this.done = false;
      this.description = initialDescription;
    }

    toggledone() {
      this.done = !this.done;
    }

    editDescription(newDescription) {
      this.description = newDescription;
    }
  }

  function render() {
    checkState();

    //empty todo-list
    while (listTodos.children.length > 0) {
      listTodos.children[0].remove();
    }

    //render todos to display
    for (let item of todoList) {
      const valid =
        (showTodo && showDone) || //show all
        (showTodo && !item.done) || //show todo
        (showDone && item.done); //show done

      if (valid) renderItem(item);
    }

    //update footer
    statusTodos.innerHTML = String(amountTodo) + " items left";
  }

  function renderItem(item) {
    currentList.push(document.createElement("li"));

    const currentItem = currentList[currentList.length - 1];
    currentItem.classList.add("row");
    listTodos.append(currentItem);

    const itemStatus = document.createElement("input");
    itemStatus.type = "checkbox";
    itemStatus.value = item.done;
    currentItem.append(itemStatus);

    const itemDescription = document.createElement("span");
    itemDescription.innerHTML = item.description;
    itemDescription.classList.add("todo-description");
    itemDescription.addEventListener("dblclick", event => {
      event.stopPropagation();
      editItem(item, itemDescription);
    });
    currentItem.append(itemDescription);

    const itemDelete = document.createElement("button");
    itemDelete.addEventListener("click", event => {
      todoList.splice(todoList.indexOf(item), 1);
      render();

      event.stopPropagation();
    });
    itemDelete.innerHTML = "delete";
    currentItem.append(itemDelete);
  }

  function checkState() {
    amountTodo = todoList.reduce((count, todoItem) => {
      return count + !todoItem.done;
    }, 0);
    amountDone = todoList.length - amountTodo;
  }

  function addNewTodo() {
    todoList.push(new todoItem(inputTodo.value));
  }

  function editItem(item, itemDescription) {
    const newInput = document.createElement("input");
    newInput.type = "text";
    newInput.classList.add("todo-description");
    newInput.onfocus = "this.select()";
    newInput.value = itemDescription.innerHTML;

    newInput.addEventListener("keyup", event => {
      if (event.code === "Enter") {
        item.description = newInput.value;
        newInput.value = itemDescription.innerHTML;
        render();
      }
    });

    itemDescription.parentNode.replaceChild(newInput, itemDescription);
    newInput.select();
  }

  toggleAll.addEventListener("click", event => {
    console.log("here");
    //toggleValue should be true if todoList contains a not-done item
    const toggleValue = todoList.some(item => item.done === false);
    todoList.forEach(item => {
      item.done = toggleValue;
    });

    render();

    event.stopPropagation();
  });

  inputTodo.addEventListener("keyup", event => {
    if (event.code === "Enter") {
      addNewTodo();
      render();
      inputTodo.value = "";

      event.stopPropagation();
    }
  });

  showAll.addEventListener("click", event => {
    showTodo = true;
    showDone = true;
    render();

    event.stopPropagation();
  });

  showActive.addEventListener("click", event => {
    showTodo = true;
    showDone = false;
    render();

    event.stopPropagation();
  });

  showCompleted.addEventListener("click", event => {
    showTodo = false;
    showDone = true;
    render();

    event.stopPropagation();
  });
}

export { app }