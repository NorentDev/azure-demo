import { fs } from 'fs';

const jsonModel = {
  readData: function (filename) {
    return JSON.parse(fs.readFileSync(filename));
  }
}

export { jsonModel };