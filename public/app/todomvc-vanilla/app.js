import { createElement } from '/js/helper.js';

class TodoItem {
  constructor(parent) {}
}

class Model {
  constructor(name) {
    this.name = name;
    this._tempTodoList = undefined; //for proper funtionality of getTodoList()
  }

  _copyTodo(item) {
    return JSON.parse(JSON.stringify(item));
    //WARNING: maybe not the best way;
    //TODO: find other ways to clone
  }

  getLocal(name) {
    return window.localStorage.getItem(name);
  }

  setLocal(name, data) {
    window.localStorage.setItem(name, data);
  }

  getTodoList() {
    return this._tempTodoList || JSON.parse(this.getLocal(this.name)) || [];
  }

  setTodoList(newTodoList) {
    this._tempTodoList = newTodoList;
    this.setLocal(this.name, JSON.stringify(newTodoList));
  }

  addToList(item) {
    const todoList = this.getTodoList();
    todoList.push(item);
    this.setTodoList(todoList);
  }

  addTodo(description) {
    const newTodo = {
      id: Date.now(),
      description: description,
      closed: false
    };
    this.addToList(newTodo);

    console.log('added: ' + newTodo.description);
    return newTodo;
  }

  editTodo(id, newDescription) {
    const editTodoWithId = (item) => {
      if (id === item.id) {
        const newItem = this._copyTodo(item);
        newItem.description = newDescription;
        return newItem;
      }
      return item;
    };

    const oldTodoList = this.getTodoList();
    const newTodoList = oldTodoList.map(editTodoWithId);

    this.setTodoList(newTodoList);
    console.log('description[' + id + '] edited: ' + newDescription);
  }

  removeTodo(id) {
    const hasDifferentId = (item) => id !== item.id;

    const oldTodoList = this.getTodoList();
    const newTodoList = oldTodoList.filter(hasDifferentId);

    this.setTodoList(newTodoList);
    console.log('removed id: ' + id);
  }

  toggleTodo(id) {
    const toggleTodoWithId = (item) => {
      if (id === item.id) {
        const newItem = this._copyTodo(item);
        newItem.closed = !item.closed;

        console.log('toggled item: ' + newItem.closed);
        return newItem;
      }
      return item;
    };

    const oldTodoList = this.getTodoList();
    const newTodoList = oldTodoList.map(toggleTodoWithId);

    this.setTodoList(newTodoList);
  }
}

class View {
  constructor(parent) {
    this.parent = parent;
    this.elements = this._createAppElements(parent);
  }

  _createAppElements(parent) {
    const createTodoHeader = () => {
      const todoHeader = {};
      todoHeader.main = createElement('section', 'header', parent);

      const createAllTodosToggle = () => {
        const allTodosToggle = createElement(
          'button',
          'toggle-all',
          todoHeader.main
        );
        allTodosToggle.innerHTML = 'check all';

        return allTodosToggle;
      };
      const createNewTodoTextbox = () => {
        const newTodoTextbox = createElement(
          'input',
          'new-todo',
          todoHeader.main
        );
        newTodoTextbox.type = 'text';
        newTodoTextbox.placeholder = 'new todo';

        return newTodoTextbox;
      };

      todoHeader.allTodosToggle = createAllTodosToggle();
      todoHeader.newTodoTextbox = createNewTodoTextbox();

      return todoHeader;
    };
    const createEmptyTodoList = () => {
      const todoList = {};
      todoList.main = createElement('ul', 'todo-list', parent);

      return todoList;
    };
    const createTodoFooter = () => {
      const todoFooter = {};
      todoFooter.main = createElement('section', 'footer', parent);

      const createStatusIndicator = () => {
        const statusIndicator = {};
        statusIndicator.main = createElement('span', 'status', todoFooter.main);
        statusIndicator.innerHTML = '0/0 closed';
        return statusIndicator;
      };
      const createFilterButtons = () => {
        const filterButtons = {};
        filterButtons.main = createElement('div', 'filters', todoFooter.main);

        filterButtons.showAll = createElement(
          'button',
          'show-all',
          filterButtons.main
        );
        filterButtons.showAll.innerHTML = 'show all';

        filterButtons.showOpen = createElement(
          'button',
          'show-open',
          filterButtons.main
        );
        filterButtons.showOpen.innerHTML = 'show open';

        filterButtons.showClosed = createElement(
          'button',
          'show-closed',
          filterButtons.main
        );
        filterButtons.showClosed.innerHTML = 'show closed';
      };
      const createClearClosedButton = () => {
        const clearClosedButton = {};
        clearClosedButton.main = createElement(
          'button',
          'clear-all',
          todoFooter.main
        );

        clearClosedButton.innerHTML = 'clear closed';
      };

      todoFooter.statusIndicator = createStatusIndicator();
      todoFooter.filterButtons = createFilterButtons();
      todoFooter.clearClosedButton = createClearClosedButton();

      return todoFooter;
    };

    const elements = {};
    elements.todoHeader = createTodoHeader();
    elements.todoList = createEmptyTodoList();
    elements.todoFooter = createTodoFooter();

    return elements;
  }

  get _newTodoDescription() {
    const elNewTodoTextbox = this.elements.todoHeader.newTodoTextbox; //_createAppElements()
    return elNewTodoTextbox.value;
  }

  _setElementId(element, id) {
    element.idTodo = id;
    return element;
  }

  _getElementId(element) {
    return element.idTodo;
  }

  _clearValue(element) {
    element.value = '';
  }

  renderTodoList(todoList) {
    todoList.forEach(this.renderTodoItem.bind(this));
  }

  renderTodoItem(todoItem) {
    const elTodoList = this.elements.todoList.main;

    const elTodoItem = createElement('li', 'todo-item row', elTodoList);
    this._setElementId(elTodoItem, todoItem.id);

    this.createElToggle(elTodoItem, todoItem.closed);
    this.createElDescription(elTodoItem, todoItem.description);
    this.createElDeleteButton(elTodoItem);
  }

  createElToggle(parent, checked) {
    const elToggle = createElement('input', 'todo-toggle', parent);
    elToggle.type = 'checkbox';
    elToggle.checked = checked;

    return elToggle;
  }

  createElDescription(parent, description) {
    const elDescription = createElement('span', 'todo-description', parent);
    elDescription.innerHTML = description;
    elDescription.addEventListener('dblclick', this.makeElDescriptionEditable);

    return elDescription;
  }

  createElDeleteButton(parent) {
    const elDeleteButton = createElement('button', 'todo-delete', parent);
    elDeleteButton.innerHTML = '✕';

    return elDeleteButton;
  }

  makeElDescriptionEditable(event) {
    const elDescription = event.target;
    const elParent = event.target.parentElement;
    const elEditableDescription = createElement('input', 'todo-edit', elParent);

    elEditableDescription.type = 'text';
    elParent.replaceChild(elEditableDescription, elDescription);

    elEditableDescription.value = elDescription.innerHTML;
    elEditableDescription.select();
  }

  applyNewDescription(event) {
    const elParent = event.target.parentElement;
    const elDescriptionEdited = event.target;
    const newDescription = elDescriptionEdited.value;

    const elNewDescription = this.createElDescription(elParent, newDescription);
    elParent.replaceChild(elNewDescription, elDescriptionEdited);
  }

  bindAddTodo(handle) {
    const elNewTodoTextbox = document.querySelector('.new-todo');
    const addTodo = (event) => {
      if (event.key === 'Enter') {
        event.stopPropagation();
        handle(this._newTodoDescription);
        this._clearValue(elNewTodoTextbox);
      }
    };

    elNewTodoTextbox.addEventListener('keyup', addTodo);
  }

  bindRemoveTodo(handle) {
    const elTodoList = document.querySelector('.todo-list');
    const removeTodoIfValid = (event) => {
      const element = event.target;

      if (element.classList.contains('todo-delete')) {
        event.stopPropagation();
        const elTodoItem = element.parentElement;
        const id = this._getElementId(elTodoItem);
        handle(id);
        elTodoItem.remove();
      }
    };

    elTodoList.addEventListener('click', removeTodoIfValid);
  }

  bindToggleTodo(handle) {
    const elTodoList = document.querySelector('.todo-list');
    const toggleTodoIfValid = (event) => {
      const element = event.target;

      if (element.classList.contains('todo-toggle')) {
        event.stopPropagation();
        const id = this._getElementId(event.target.parentElement);
        handle(id);
      }
    };

    elTodoList.addEventListener('click', toggleTodoIfValid);
  }

  bindEditTodo(handle) {
    const elTodoList = document.querySelector('.todo-list');
    const editTodoIfValid = (event) => {
      const eventFromTodoEdit = event.target.classList.contains('todo-edit');
      const enterPressed = event.key === 'Enter';

      if (eventFromTodoEdit && enterPressed) {
        event.stopPropagation();
        const id = this._getElementId(event.target.parentElement);
        const newDescription = event.target.value;
        handle(id, newDescription);
        this.applyNewDescription(event);
      }
    };

    elTodoList.addEventListener('keyup', editTodoIfValid);
  }
}

class Controller {
  constructor(model, view) {
    this.model = model;
    this.view = view;

    this.initTodoListFromModel();
    this.initEventListeners();
  }

  initTodoListFromModel() {
    const todoList = this.model.getTodoList();
    this.view.renderTodoList(todoList);
  }

  initEventListeners() {
    this.view.bindAddTodo(this.addTodo.bind(this));
    this.view.bindRemoveTodo(this.removeTodo.bind(this));
    this.view.bindToggleTodo(this.toggleTodo.bind(this));
    this.view.bindEditTodo(this.editTodo.bind(this));
  }

  addTodo(newDescription) {
    const todoItem = this.model.addTodo(newDescription);
    this.view.renderTodoItem(todoItem);
  }

  removeTodo(id) {
    this.model.removeTodo(id);
  }

  toggleTodo(id) {
    this.model.toggleTodo(id);
  }

  editTodo(id, newDescription) {
    this.model.editTodo(id, newDescription);
  }
}

class ViewDummy {
  constructor(elParent) {
    console.log('viewDummy constructed');
  }

  renderTodoList() {}

  bindAddTodo() {}

  bindRemoveTodo() {}

  bindToggleTodo() {}

  bindEditTodo() {}
}

class Test {
  constructor(model, view, controller) {
    this.model = model;
    this.view = view;
    this.controller = controller;

    this.startTestSequence();
  }

  startTestSequence() {
    const tests = [
      this.modelSetGetLocal,
      this.modelAddTodo,
      this.modelRemoveTodo,
      this.modelToggleTodo,
      this.modelEditTodo
    ].map((test) => test.bind(this));

    this._executeTests(tests);
  }

  _executeTests(tests) {
    const logTest = (handleTest) => {
      const testPassed = handleTest(handleTest.name);
      console.log(
        'test.' +
          handleTest.name +
          '(): ' +
          (testPassed ? 'passed' : 'not passed')
      );
    };

    tests.forEach(logTest);
  }

  _registerResult(description, passed) {
    return { description, passed };
  }

  _concludeResults(tests) {
    const notAllTestsPassed = tests.some((test) => !test.passed);

    if (notAllTestsPassed) {
      tests.forEach((test) => console.log(test.description, test.passed));
      return false;
    }
    return true;
  }

  _delayForUniqueId() {
    for (let i = 0; i < 99999; i++) {
      ("I couldn't find a good way to create some delay");
    }
  }

  modelSetGetLocal(name) {
    /*
     * localStorage should work well
     */
    const changeValue = (oldValue) => {
      const lengthMaximum = 100;
      const indexSlice = Math.floor(lengthMaximum / 2);
      const valueConcat = 'test';
      if (oldValue == null) return valueConcat;
      if (oldValue.length > lengthMaximum) return oldValue.slice(indexSlice);
      return oldValue + valueConcat;
    };

    const previousValue = this.model.getLocal(name);
    const newValue = changeValue(previousValue);
    this.model.setLocal(name, newValue);
    const actualNewValue = this.model.getLocal(name);

    return newValue === actualNewValue;
  }

  modelAddTodo(description) {
    /*
     * when a new todo is added,
     * the todo-list is updated such that:
     * it is consistent with the old todo-list, and
     * it is consistent with the new todo-item
     */
    const previousTodoList = this.model.getTodoList();

    this.model.addTodo(description);
    let newTodoList = this.model.getTodoList();

    const addedTodo = newTodoList.pop();

    const tests = [];
    tests.push(
      this._registerResult(
        'new consistent with previous: ',
        JSON.stringify(newTodoList) === JSON.stringify(previousTodoList)
      )
    );
    tests.push(
      this._registerResult(
        'new consistent with addItem: ',
        description === addedTodo.description
      )
    );

    return this._concludeResults(tests);
  }

  modelRemoveTodo() {
    this.model.addTodo('remove1');
    this._delayForUniqueId();
    this.model.addTodo('remove2');

    const oldTodoList = this.model.getTodoList();
    const removedTodo = oldTodoList[oldTodoList.length - 1];
    this.model.removeTodo(removedTodo.id);

    const expectedTodoList = oldTodoList.slice(0, oldTodoList.length - 1);
    const newTodoList = this.model.getTodoList();

    const tests = [];
    tests.push(
      this._registerResult(
        'succesful removal of last item: ',
        JSON.stringify(expectedTodoList) === JSON.stringify(newTodoList)
      )
    );

    return this._concludeResults(tests);
  }

  modelToggleTodo() {
    const getTodoItem = () => {
      const currentTodoList = this.model.getTodoList();
      return currentTodoList[currentTodoList.length - 1];
    };

    this.model.addTodo('toggle1');
    this._delayForUniqueId();
    this.model.addTodo('toggle2');

    const oldTodoItem = getTodoItem();
    this.model.toggleTodo(oldTodoItem.id);
    const newTodoItem = getTodoItem();

    const tests = [];
    tests.push(
      this._registerResult(
        'succesful toggle of item: ',
        oldTodoItem.closed !== newTodoItem.closed
      )
    );

    return this._concludeResults(tests);
  }

  modelEditTodo() {
    const getTodoItem = () => {
      const currentTodoList = this.model.getTodoList();
      return currentTodoList[currentTodoList.length - 1];
    };

    this.model.addTodo('edit1');
    this._delayForUniqueId();
    this.model.addTodo('edit2');

    const oldTodoItem = getTodoItem();
    const newDescription = 'new description for testing';
    this.model.editTodo(oldTodoItem.id, newDescription);
    const newTodoItem = getTodoItem();

    const tests = [];
    tests.push(
      this._registerResult(
        'item description has changed: ',
        oldTodoItem.description !== newTodoItem.description
      )
    );
    tests.push(
      this._registerResult(
        'item description has been modified correctly: ',
        newTodoItem.description === newDescription
      )
    );

    return this._concludeResults(tests);
  }
}

export function startTodoListIn(parent) {
  return new Controller(new Model(), new View(parent));
}

export function testTodoList() {
  const elTestParent = createElement('div'),
    testModel = new Model('testTodoList'),
    testView = new ViewDummy(elTestParent),
    testController = new Controller(testModel, testView),
    testInstance = new Test(testModel, testView, testController);

  return testInstance;
}
