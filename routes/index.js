const express = require('express');
const fs = require('fs');
const router = express.Router();
//should be separated in the model
const getDataFromJsonFile = (filename) => JSON.parse(fs.readFileSync(filename));


/* prepare data-structures*/
const layoutData = getDataFromJsonFile('./json/header-footer.json');
const articleData = {};

/* GET home page. */
router.get('/', function (req, res, next) {
  articleData.index = getDataFromJsonFile('./json/index-article.json');
  console.log('home page requested');
  res.render('article.pug', {
    article: articleData.index,
    navigation: layoutData.navigation,
    footer: layoutData.footer
  });
});

router.get('/app-todomvc-vanilla', function (req, res, next) {
  res.render('app-js.pug', {
    jsFile: '/app/todomvc-vanilla/init.js',
    navigation: layoutData.navigation,
    footer: layoutData.footer
  });
});

router.get('/app-todo-vanilla', function (req, res, next) {
  res.render('app-js.pug', {
    jsFile: '/app/todo-vanilla/init.js',
    navigation: layoutData.navigation,
    footer: layoutData.footer
  });
});

router.get('/app-snow', function (request, response, next) {
  response.render('app-js.pug', {
    jsFile: '/app/snow/init.js',
    navigation: layoutData.navigation,
    footer: layoutData.footer
  });
});

module.exports = router;
