import { app } from './app.js';
import { createElement, getParentScriptElement } from '/js/helper.js';

{
  function createHeaderElements(parentElement) {
    const headerDiv = createElement('div', 'header row', parentElement);

    const toggleAllButton = createElement('button', 'toggle-all', headerDiv);
    toggleAllButton.innerHTML = "toggle all";

    const todoTextInput = createElement('input', 'input-todo', headerDiv);
    todoTextInput.type = 'text';
  }

  function createTodolistElement(parentElement) {
    const listTodoEl = createElement('ul', 'list-todos', parentElement);
  }

  function createFooterElements(parentElement) {
    function createButtons(buttonContainer) {
      const showAllButton = createElement('button', 'show-all', buttonContainer);
      showAllButton.innerHTML = 'all';

      const showActiveButton = createElement('button', 'show-active', buttonContainer);
      showActiveButton.innerHTML = 'active';

      const showCompletedButton = createElement('button', 'show-completed', buttonContainer);
      showCompletedButton.innerHTML = 'completed';
    }

    const footerDiv = createElement('div', 'footer', parentElement);

    const todoStatusEl = createElement('div', 'status-todos', footerDiv);
    todoStatusEl.innerHTML = '0 items left';

    const footerButtonsDiv = createElement('div', 'footer-buttons', footerDiv);
    createButtons(footerButtonsDiv);

    const emptyDiv = createElement('div', 'empty', footerDiv);
  }

  //dom-elements rewritten from original html file
  const mainDiv = getParentScriptElement('todo-vanilla/init.js');
  mainDiv.classList.add('todo-app');
  createHeaderElements(mainDiv);
  createTodolistElement(mainDiv);
  createFooterElements(mainDiv);

  //execution
  app.start(mainDiv);
}