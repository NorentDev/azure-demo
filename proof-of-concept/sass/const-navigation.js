const navigation = [
  {
    id: 'apps',
    link: '#apps',
    title: 'apps',
    sub: [
      {
        id: 'todo',
        title: 'todo'
      }
    ]
  },
  {
    id: 'about me',
    link: '#about_me',
    title: 'about me',
    sub: [
      {
        id: 'curriculum vitae',
        title: 'curriculum vitae'
      }
    ]
  }
];

const footer = ['made by Norent Khy', 'norent.dev@gmail.com'];

module.exports = { navigation, footer };
