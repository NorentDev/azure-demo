// file system module to perform file operations
const fs = require('fs');
const path = require('path');

// json data
//const jsonObj = require(path.join(__dirname, 'const-navigation.js'));

const filename = path.join(__dirname, 'index-article.json');
const jsonObj = {
  title: 'Value at the right places',
  content:
    'There is truth in value. ' +
    'Man can live in absence of worries about the future and past. ' +
    'This achievement lies in the formation of timeless values. ' +
    'These values regard the direction in which things are going. ' +
    'They regard not what is perceived as good and bad. ' +
    'The idea that man can perceive good and bad is an illusion. ' +
    'A life without illusion has values, which are held up high. '
};

const jsonContent = JSON.stringify(jsonObj);
console.log(jsonContent);

fs.writeFile(filename, jsonContent, 'utf8', function(err) {
  if (err) {
    console.log('An error occured while writing JSON Object to File.');
    return console.log(err);
  }

  console.log('JSON file has been saved.');
});
