const renderSass = () => {
  const config = {
    file: '../proto/style.sass',
    outFile: '../proto/sass.css'
  };

  const callback = function(err, result) {
    if (!err) {
      // No errors during the compilation, write this result on the disk
      fs.writeFile(yourPathTotheFile, result.css, function(err) {
        if (!err) {
          //file written on disk
        }
      });
    }
  };

  sass.render(config, callback);
};

module.export = renderSass;
