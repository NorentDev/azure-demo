const sass = require('node-sass');
const fs = require('fs');
const path = require('path');

const renderSass = () => {
  const inFile = path.join(__dirname, 'style.sass');
  const outFile = path.join('./public/css/style-sass.css');
  const config = {
    file: inFile,
    outFile: outFile
  };

  const callback = function (errSass, result) {
    if (!errSass) {
      fs.writeFile(outFile, result.css, function (errFs) {
        if (!errFs) {
          const fileName = path.basename(__filename);
          console.log(fileName + ': ' + inFile + ' rendered to ' + outFile);
        } else {
          console.log('fs err: ' + errFs);
        }
      });
    } else {
      console.log('sass err: ' + errSass);
    }
  };

  sass.render(config, callback);
};

renderSass();
