function createElement(type, className, parent) {
  const element = document.createElement(type);
  if (className) {
    element.className = className;
  }
  if (parent) {
    parent.append(element);
  }

  return element;
}

function getParentScriptElement(partialPath) {
  const scriptEls = document.getElementsByTagName('script');

  const thisScriptEl =
    Array.prototype.filter.call(
      scriptEls,
      (scriptEl) => scriptEl.src.includes(partialPath)
    )[0];

  return thisScriptEl.parentElement;
}

export { createElement, getParentScriptElement };