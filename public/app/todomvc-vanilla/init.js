import { startTodoListIn, testTodoList } from './app.js';
import { getParentScriptElement } from '/js/helper.js';

const mainDiv = getParentScriptElement('todomvc-vanilla/init.js');
mainDiv.classList.add("todomvc-vanilla-app");
testTodoList();
startTodoListIn(mainDiv);
