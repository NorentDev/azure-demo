path = require('path');

function logObject(obj) {
  function getContentKey(obj, key) {
    const hasNoKeys = Object.keys(obj).length === 0;
    if (hasNoKeys) {
      return; //'  ' + key + ': ' + obj[key] + '\n';
    } else {
      return getContentObject(obj[key]);
    }
  }

  function getContentObject(obj) {
    const keys = Object.keys(obj);
    const hasKeys = keys.length > 0;
    const noContent = '';
    depth++;
    if (depth > 3) {
      return;
    }

    console.log('±±±±±\n' + keys);

    if (!hasKeys) {
      return noContent;
    } else {
      let content = '{\n';
      for (key of keys) {
        content += getContentKey(obj, key);
      }
      content += '}\n';

      return content;
    }
  }

  let depth = 0;

  contentObject = getContentObject(obj);
  console.log('±±±±±\n' + contentObject);
}

const example = {};
example.join = path.join(__dirname, '..', 'test.jsdf');
example.deeper = {};
example.deeper.a = 1;

logObject(example);

/*
if haskeys() 
  getDeeperContent
else

*/
