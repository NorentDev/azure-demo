import { createElement } from '/js/helper.js';

//https://codepen.io/nrninsane/pen/eYmpdjE

{
  const scriptEls = document.getElementsByTagName('script');

  const thisScriptEl =
    Array.prototype.filter.call(
      scriptEls,
      (scriptEl) => scriptEl.src.includes('snow/init.js')
    )[0];

  const parentDiv = thisScriptEl.parentElement;
  parentDiv.classList.add('snow-app');

  const amountSnowDivs = 100;

  for (let i = 0; i < amountSnowDivs; i++) {
    createElement('div', 'snow', parentDiv);
  }
}